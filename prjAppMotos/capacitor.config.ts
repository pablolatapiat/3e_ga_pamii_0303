import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'prjAppMotos',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
