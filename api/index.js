const mysql = require('mysql2')
const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const app = express()
const banco = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'minas',
    database: ''
})

app.use(cors())
const corsOptions = {
    origin: '',
    optionsSuccessStatus: 200
}

app.get('/', (req, res) => {
    return res.status(200).json({
        'message': 'Servidor funcionando'
    })
})

app.listen(3000, () => { console.log('Servidor executando...') })    
